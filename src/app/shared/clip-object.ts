export interface ClipObject {
  views: number;
  thumbsUp: number;
  thumbsDown: number;
}
