import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { ClipObject } from '../clip-object';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  private _clipObject: AngularFireObject<ClipObject>;

  constructor(db: AngularFireDatabase) {
    this._clipObject = db.object('/clip-data'); // init clip Data object from Firebase Realtime Database
  }

  get clipObject(): Observable<ClipObject> {
    // Observable on Realtime Database changes
    return this._clipObject.valueChanges();
  }

  update({ views, thumbsUp, thumbsDown }: ClipObject) {
    // update Firebase Realtime Database with new data
    this._clipObject.update({ views, thumbsUp, thumbsDown });
  }
}
