import { Component } from '@angular/core';
import { ClipObject } from './shared/clip-object';
import { FetchDataService } from './shared/services/fetch-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  private currTimeSec = 0;
  private _duration = 0;
  public clipData: ClipObject = { views: 0, thumbsUp: 0, thumbsDown: 0 };
  public isLiked: boolean;
  public isDisliked: boolean;
  private isViewsUpdated: boolean;

  constructor(private dataService: FetchDataService) {
    this.dataService.clipObject.subscribe((obj: ClipObject) => {
      // get clip data located on the server
      if (!obj) {
        return;
      }
      Object.assign(this.clipData, obj);
    });
  }

  get currTimeStr(): string {
    // current clip time
    return this.secondsConverter(this.currTimeSec);
  }

  get duration(): string {
    // whole clip duration
    return this.secondsConverter(this._duration);
  }

  /**
   * Convert seconds into prettier format: HH:MM:SS
   * Return string (HH:MM:SS)
   * @param sec A numeric value of seconds.
   */
  private secondsConverter(sec: number): string {
    return new Date(sec * 1000).toISOString().substr(11, 8);
  }

  public playPause(playerEl: HTMLVideoElement) {
    return playerEl.paused ? playerEl.play() : playerEl.pause();
  }

  public ended() {
    this.isViewsUpdated = false;  // allow to count whole watched clips without refresh the page
  }

  public loaded(duration: number) {
    // set whole clip duration on clip loaded
    return this._duration = duration;
  }

  public onCurrTimeUpdate(currTime: number) {
    // save current time in seconds
    this.currTimeSec = currTime;
    if (!this.isViewsUpdated && currTime >= this._duration * 25 / 100) {
      // update views only after watching of 25% of the clip
      // the same can be done for likes/dislikes if necessary - to prevent liking before watching at least 25%
      this.isViewsUpdated = true;
      this.clipData.views++;
      return this.dataService.update(this.clipData);
    }
  }

  public handleLikes(flag: 'up' | 'down') {
    if (flag === 'up') {
      // thumbs up is clicked
      if (this.isLiked) {
        // unselect thumbs up
        this.isLiked = false;
        this.clipData.thumbsUp--;
      } else {
        this.clipData.thumbsUp++;
        this.isLiked = true;
        if (this.isDisliked) {
          // unselect thumbs down if it was clicked before
          this.isDisliked = false;
          this.clipData.thumbsDown--;
        }
      }
      return this.dataService.update(this.clipData);
    }
    // thumbs down is clicked
    if (this.isDisliked) {
      // unselect thumbs down
      this.isDisliked = false;
      this.clipData.thumbsDown--;
    } else {
      this.isDisliked = true;
      this.clipData.thumbsDown++;
      if (this.isLiked) {
        // unselect thumbs up if it was clicked before
        this.isLiked = false;
        this.clipData.thumbsUp--;
      }
    }
    return this.dataService.update(this.clipData);
  }
}
