// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB-oD5jkhVEFeNYomEFB6ISdeP65TKJwEY',
    authDomain: 'video-player-26727.firebaseapp.com',
    databaseURL: 'https://video-player-26727.firebaseio.com',
    projectId: 'video-player-26727',
    storageBucket: '',
    messagingSenderId: '1027358524476',
    appId: '1:1027358524476:web:39bfdbb494a48914'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
